# RDP Generator Tool

Repository for COMP3931 Individual Project Recursive Descent Parser Generator

Created by Jacob Westwell.

## Project Report

The [Final Report](https://gitlab.com/sc17jalw/rdp-generator-tool/-/wikis/uploads/61a8c732b5fca3bcc8e06d47b1e689af/Final_Report.pdf) detailing the development, testing and findings of the RDP Generator Tool can be found in the project Wiki using the following link:

https://gitlab.com/sc17jalw/rdp-generator-tool/-/wikis/uploads/61a8c732b5fca3bcc8e06d47b1e689af/Final_Report.pdf

## User Instructions

### Generating a Parser

1. Copy all the files in the /src directory to another directory to work inside.
2. Compile all the files in the new directory with the command:

    `javac *.java`
    
3. Run the ParserGenerator class, passing in the path to the grammar file to be processed. Use the following command:

    `java ParserGenerator *PATH TO GRAMMAR FILE*`

The tool will create a new directory with the name of the grammar file inputted to the tool. This directory will contain all of the files necessary to parse a language file with the grammar specified.
    
### Using the Generated Parser

1. Navigated to the directory that the tool created with the name of the grammar file which was used to generate the parser.
2. Compile all of the files with the command:

    `javac *.java`
    
3. Run the TreeGenerator class, passing in the path to the language file to be processed. Use the following command:

    `java TreeGenerator *PATH TO LANGUAGE FILE*`
    
The tool will then print a parse tree to the console that represents the language file inputted.
