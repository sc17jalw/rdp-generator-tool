class Parser {
	private Lexer lexer;

	private void error(Token token, String message) {
		System.out.println("ERROR: " + message);
		System.out.println("TOKEN: " + token.toString());
		System.exit(1);
	}

	public Tree parse(Lexer lexer) {
		// Call start symbol.
		this.lexer = lexer;
		Tree outputTree = new Tree("class_declaration", true);
		class_declaration(outputTree);
		return outputTree;
	}

	// Production rule.
	private void class_declaration(Tree outputTree) {
		Token token = null;
		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("class")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"class\".");
		}

		// Terminal.
		token = lexer.getNextToken();
		outputTree.addChild(token.getLexeme(), false);

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("{")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"{\".");
		}

		// Repeated Statement.
		while (lexer.peekNextToken(0).getLexeme().equals("member")) {

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("member")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"member\".");
		}

		// Non terminal
		outputTree.addChild("member_declaration", true);
		member_declaration(outputTree.getChild("member_declaration"));

		}

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("}")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"}\".");
		}

	}

	// Production rule.
	private void member_declaration(Tree outputTree) {
		Token token = null;
		// Non terminal
		outputTree.addChild("variable_declaration", true);
		variable_declaration(outputTree.getChild("variable_declaration"));

		// Non terminal
		outputTree.addChild("subroutine", true);
		subroutine(outputTree.getChild("subroutine"));

	}

	// Production rule.
	private void variable_declaration(Tree outputTree) {
		Token token = null;
		// Optional Statement.
		token = lexer.peekNextToken(0);
		if (token.getLexeme().equals("var")) {

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("var")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"var\".");
		}

		// Terminal.
		token = lexer.getNextToken();
		outputTree.addChild(token.getLexeme(), false);

		// Terminal.
		token = lexer.getNextToken();
		outputTree.addChild(token.getLexeme(), false);

		// Repeated Statement.
		while (lexer.peekNextToken(0).getLexeme().equals(",")) {

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals(",")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \",\".");
		}

		// Terminal.
		token = lexer.getNextToken();
		outputTree.addChild(token.getLexeme(), false);

		}

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals(";")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \";\".");
		}

		}

	}

	// Production rule.
	private void subroutine(Tree outputTree) {
		Token token = null;
		// Optional Statement.
		token = lexer.peekNextToken(0);
		if (token.getLexeme().equals("func")) {

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("func")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"func\".");
		}

		// Terminal.
		token = lexer.getNextToken();
		outputTree.addChild(token.getLexeme(), false);

		// Terminal.
		token = lexer.getNextToken();
		outputTree.addChild(token.getLexeme(), false);

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("(")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"(\".");
		}

		// Terminal.
		token = lexer.getNextToken();
		outputTree.addChild(token.getLexeme(), false);

		// Terminal.
		token = lexer.getNextToken();
		outputTree.addChild(token.getLexeme(), false);

		// Repeated Statement.
		while (lexer.peekNextToken(0).getLexeme().equals(",")) {

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals(",")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \",\".");
		}

		// Terminal.
		token = lexer.getNextToken();
		outputTree.addChild(token.getLexeme(), false);

		// Terminal.
		token = lexer.getNextToken();
		outputTree.addChild(token.getLexeme(), false);

		}

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals(")")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \")\".");
		}

		// Non terminal
		outputTree.addChild("subroutine_body", true);
		subroutine_body(outputTree.getChild("subroutine_body"));

		}

	}

	// Production rule.
	private void subroutine_body(Tree outputTree) {
		Token token = null;
		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("{")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"{\".");
		}

		// Repeated Statement.
		while (lexer.peekNextToken(0).getLexeme().equals("line")) {

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("line")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"line\".");
		}

		// Non terminal
		outputTree.addChild("statement", true);
		statement(outputTree.getChild("statement"));

		// Non terminal
		outputTree.addChild("control_statement", true);
		control_statement(outputTree.getChild("control_statement"));

		}

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("}")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"}\".");
		}

	}

	// Production rule.
	private void statement(Tree outputTree) {
		Token token = null;
		// Optional Statement.
		token = lexer.peekNextToken(0);
		if (token.getLexeme().equals("statement")) {

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("statement")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"statement\".");
		}

		// Non terminal
		outputTree.addChild("variable_declaration", true);
		variable_declaration(outputTree.getChild("variable_declaration"));

		// Non terminal
		outputTree.addChild("assignment_statement", true);
		assignment_statement(outputTree.getChild("assignment_statement"));

		}

	}

	// Production rule.
	private void assignment_statement(Tree outputTree) {
		Token token = null;
		// Optional Statement.
		token = lexer.peekNextToken(0);
		if (token.getLexeme().equals("assign")) {

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("assign")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"assign\".");
		}

		// Terminal.
		token = lexer.getNextToken();
		outputTree.addChild(token.getLexeme(), false);

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("=")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"=\".");
		}

		// Terminal.
		token = lexer.getNextToken();
		outputTree.addChild(token.getLexeme(), false);

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals(";")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \";\".");
		}

		}

	}

	// Production rule.
	private void control_statement(Tree outputTree) {
		Token token = null;
		// Optional Statement.
		token = lexer.peekNextToken(0);
		if (token.getLexeme().equals("control")) {

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("control")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"control\".");
		}

		// Non terminal
		outputTree.addChild("if_statement", true);
		if_statement(outputTree.getChild("if_statement"));

		// Non terminal
		outputTree.addChild("while_statement", true);
		while_statement(outputTree.getChild("while_statement"));

		}

	}

	// Production rule.
	private void if_statement(Tree outputTree) {
		Token token = null;
		// Optional Statement.
		token = lexer.peekNextToken(0);
		if (token.getLexeme().equals("if")) {

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("if")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"if\".");
		}

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("(")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"(\".");
		}

		// Non terminal
		outputTree.addChild("condition", true);
		condition(outputTree.getChild("condition"));

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals(")")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \")\".");
		}

		// Non terminal
		outputTree.addChild("subroutine_body", true);
		subroutine_body(outputTree.getChild("subroutine_body"));

		// Optional Statement.
		token = lexer.peekNextToken(0);
		if (token.getLexeme().equals("else")) {

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("else")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"else\".");
		}

		// Non terminal
		outputTree.addChild("subroutine_body", true);
		subroutine_body(outputTree.getChild("subroutine_body"));

		}

		}

	}

	// Production rule.
	private void while_statement(Tree outputTree) {
		Token token = null;
		// Optional Statement.
		token = lexer.peekNextToken(0);
		if (token.getLexeme().equals("while")) {

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("while")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"while\".");
		}

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("(")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"(\".");
		}

		// Non terminal
		outputTree.addChild("condition", true);
		condition(outputTree.getChild("condition"));

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals(")")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \")\".");
		}

		// Non terminal
		outputTree.addChild("subroutine_body", true);
		subroutine_body(outputTree.getChild("subroutine_body"));

		}

	}

	// Production rule.
	private void condition(Tree outputTree) {
		Token token = null;
		// Terminal.
		token = lexer.getNextToken();
		outputTree.addChild(token.getLexeme(), false);

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("==")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"==\".");
		}

		// Terminal.
		token = lexer.getNextToken();
		outputTree.addChild(token.getLexeme(), false);

	}

}