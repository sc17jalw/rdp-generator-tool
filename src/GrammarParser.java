import java.util.InputMismatchException;

public class GrammarParser {
    private Lexer lexer;
    private Boolean insideConditional = false;
    private Boolean writeParser;
    private StringBuilder output = new StringBuilder();

    private void ok(Token token) {
        if (!writeParser) {
            System.out.println("\t" + token.toString() + " is okay.");
        }
    }

    private void error(String message, Token token) {
        throw new InputMismatchException("ERROR: " + message + " TOKEN: " + token.toString());
    }

    public void parseGrammar(Lexer lexer) {
        this.lexer = lexer;
        this.writeParser = false;

        while (!lexer.peekNextToken(0).getLexeme().equals("endOfFile")) {
            grammar();
        }
    }

    public String generateParser(Lexer lexer) {
        this.lexer = lexer;
        this.writeParser = true;
        this.output = new StringBuilder();

        // Class definition.
        output.append("class Parser {\n\tprivate Lexer lexer;\n\n");

        // Error method definition.
        output.append("\tprivate void error(Token token, String message) {\n");
        output.append("\t\tSystem.out.println(\"ERROR: \" + message);\n");
        output.append("\t\tSystem.out.println(\"TOKEN: \" + token.toString());\n");
        output.append("\t\tSystem.exit(1);\n\t}\n\n");

        // Parse method definition.
        output.append("\tpublic Tree parse(Lexer lexer) {\n\t\t// Call start symbol.\n\t\tthis.lexer = lexer;\n\t\tTree outputTree = new Tree(\"");
        output.append(lexer.peekNextToken(0).getLexeme()).append("\", true);\n");
        output.append("\t\t").append(lexer.peekNextToken(0).getLexeme()).append("(outputTree);\n");
        output.append("\t\treturn outputTree;\n\t}\n\n");

        while (!lexer.peekNextToken(0).getLexeme().equals("endOfFile")) {
            grammar();
        }

        output.append("}");

        return this.output.toString();
    }

    private void grammar() {
        while (lexer.peekNextToken(0).getType() == TokenType.nonTerminal) {
            rule();
        }
    }

    private void rule() {
        // Consume non-terminal token.
        Token token = lexer.getNextToken();
        if (token.getType() == TokenType.nonTerminal) {
            ok(token);
        } else {
            error("Expected non-terminal token.", token);
        }

        if (writeParser) {
            output.append("\t// Production rule.\n");
            output.append("\tprivate void ").append(token.getLexeme()).append("(Tree outputTree) {\n");
            output.append("\t\tToken token = null;\n");
        }

        // Consume :=.
        token = lexer.getNextToken();
        if (token.getLexeme().equals(":=")) {
            ok(token);
        } else {
            error("Expected := symbol.", token);
        }

        body();

        // Consume new line character.
        token = lexer.getNextToken();
        if (token.getLexeme().equals(";")) {
            ok(token);
        } else {
            error("Expected \";\" character at end of production rule.", token);
        }
        
        if (writeParser) {
            output.append("\t}\n\n");
        }
    }

    private void body() {
        while ((lexer.peekNextToken(0).getType() == TokenType.nonTerminal) ||
                (lexer.peekNextToken(0).getType() == TokenType.terminal) ||
                (lexer.peekNextToken(0).getType() == TokenType.terminalString) ||
                lexer.peekNextToken(0).getLexeme().equals("(") ||
                lexer.peekNextToken(1).getLexeme().equals("|") ||
                lexer.peekNextToken(0).getLexeme().equals("[") ||
                lexer.peekNextToken(0).getLexeme().equals("{")) {


            if ((lexer.peekNextToken(1).getLexeme().equals("|")) && (!insideConditional)) {
                conditionalExpression();
            } else if ((lexer.peekNextToken(0).getLexeme().equals("(")) && (lexer.peekNextToken(0).getType() == TokenType.symbol)) {
                priorityExpression();
            } else if ((lexer.peekNextToken(0).getLexeme().equals("[")) && (lexer.peekNextToken(0).getType() == TokenType.symbol)) {
                optionalExpression();
            } else if ((lexer.peekNextToken(0).getLexeme().equals("{")) && (lexer.peekNextToken(0).getType() == TokenType.symbol)) {
                repeatedExpression();
            } else if (lexer.peekNextToken(0).getType() == TokenType.terminal) {
                terminal();
            } else if (lexer.peekNextToken(0).getType() == TokenType.terminalString) {
                terminalString();
            } else if (lexer.peekNextToken(0).getType() == TokenType.nonTerminal) {
                Token token = lexer.getNextToken();
                ok(token);

                if (writeParser) {
                    output.append("\t\t// Non terminal\n");
                    output.append("\t\toutputTree.addChild(\"").append(token.getLexeme()).append("\", true);\n");
                    output.append("\t\t").append(token.getLexeme()).append("(outputTree.getChild(\"").append(token.getLexeme()).append("\"));\n\n");
                }
            }
        }
    }

    private void terminal() {
        insideConditional = false;
        Token token = lexer.getNextToken();
        if (token.getType() == TokenType.terminal) {
            ok(token);
        } else {
            error("Expected terminal symbol.", token);
        }

        if (writeParser) {
            output.append("\t\t// Terminal.\n");
            output.append("\t\ttoken = lexer.getNextToken();\n");
            output.append("\t\toutputTree.addChild(token.getLexeme(), false);\n\n");
        }
    }

    private void terminalString() {
        insideConditional = false;
        Token token = lexer.getNextToken();
        if (token.getType() == TokenType.terminalString) {
            ok(token);
        } else {
            error("Expected terminal string token.", token);
        }

        if (writeParser) {
            output.append("\t\t// Terminal String.\n");
            output.append("\t\ttoken = lexer.getNextToken();\n");
            output.append("\t\tif (token.getLexeme().equals(\"").append(token.getLexeme()).append("\")) {\n");
            output.append("\t\t\toutputTree.addChild(token.getLexeme(), false);\n\t\t} else {\n");
            output.append("\t\t\terror(token, \"Expected symbol \\\"").append(token.getLexeme()).append("\\\".\");\n\t\t}\n\n");
        }
    }

    private void priorityExpression() {
        insideConditional = false;
        Token token = lexer.getNextToken();
        if (token.getLexeme().equals("(")) {
            ok(token);
        }

        body();

        token = lexer.getNextToken();
        if (token.getLexeme().equals(")")) {
            ok(token);
        } else {
            error("Expected closing \")\" character.", token);
        }
    }

    private void conditionalExpression() {
        insideConditional = true;
        Token token = lexer.peekNextToken(0);

        // Check that first symbol of clause is a terminal string.
        if (token.getType() != TokenType.terminalString) {
            error("First symbol of conditional clause must be of type terminal string.", token);
        }

        if (writeParser) {
            output.append("\t\t// Conditional Statement.\n");
            output.append("\t\ttoken = lexer.peekNextToken(0);\n");
            output.append("\t\tif (token.getLexeme().equals(\"");
            output.append(token.getLexeme());
            output.append("\")) {\n\n");
        }

        body();

        token = lexer.getNextToken();
        if (token.getLexeme().equals("|")) {
            ok(token);
        }

        if (writeParser) {
            output.append("\t\t} else {\n\n");
        }

        body();

        if (writeParser) {
            output.append("\t\t}\n\n");
        }
    }

    private void optionalExpression() {
        insideConditional = false;
        Token token = lexer.getNextToken();
        if (token.getLexeme().equals("[")) {
            ok(token);
        }

        // Check that first symbol of clause is a terminal string.
        if (lexer.peekNextToken(0).getType() != TokenType.terminalString) {
            error("First symbol of optional clause must be of type terminal string.", lexer.peekNextToken(0));
        }

        if (writeParser) {
            output.append("\t\t// Optional Statement.\n");
            output.append("\t\ttoken = lexer.peekNextToken(0);\n");
            output.append("\t\tif (token.getLexeme().equals(\"").append(lexer.peekNextToken(0).getLexeme()).append("\")) {\n\n");
        }

        body();

        token = lexer.getNextToken();
        if (token.getLexeme().equals("]")) {
            ok(token);
        } else {
            error("Expected closing \"]\" character.", token);
        }

        if (writeParser) {
            output.append("\t\t}\n\n");
        }
    }

    private void repeatedExpression() {
        insideConditional = false;
        Token token = lexer.getNextToken();
        if (token.getLexeme().equals("{")) {
            ok(token);
        }

        // Check that first symbol of clause is a terminal string.
        if (lexer.peekNextToken(0).getType() != TokenType.terminalString) {
            error("First symbol of repeated clause must be of type terminal string.", lexer.peekNextToken(0));
        }

        if (writeParser) {
            output.append("\t\t// Repeated Statement.\n");
            output.append("\t\twhile (lexer.peekNextToken(0).getLexeme().equals(\"").append(lexer.peekNextToken(0).getLexeme()).append("\")) {\n\n");
        }

        body();

        token = lexer.getNextToken();
        if (token.getLexeme().equals("}")) {
            ok(token);
        } else {
            error("Expected closing \"}\" character.", token);
        }

        if (writeParser) {
            output.append("\t\t}\n\n");
        }
    }
}

