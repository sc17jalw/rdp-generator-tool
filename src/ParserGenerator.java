import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class ParserGenerator {
    public static void main(String[] args) throws IOException {
        // Get filename
        String filename = "";
        try {
            if (!args[0].equals("")) {
                filename = args[0];
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            // No filename has been provided, exit the program.
            throw new FileNotFoundException("Please enter the filename of the grammar file in the form:\njava ParserGenerator *PATH TO GRAMMAR FILE*");
        }

        // Open file and read contents to a string.
        StringBuilder inputText = new StringBuilder();
        try {
            File grammarFile = new File(filename);
            Scanner scanner = new Scanner(grammarFile);
            while (scanner.hasNextLine()) {
                inputText.append(scanner.nextLine());
                inputText.append("\n");
            }
        } catch (FileNotFoundException e) {
            System.out.println("File: " + filename + " does not exist.");
            System.exit(1);
        }

        // Create lexer object and pass file text.
        Lexer lexer = new Lexer();
        lexer.tokenize(inputText.toString());

        // Initialise parser with lexer object.
        GrammarParser parser = new GrammarParser();

        // Check grammar is correct and ready for generating source code.
        lexer.reset();
        System.out.println("Checking grammar file for format errors...\n");
        parser.parseGrammar(lexer);
        System.out.println("\nGrammar file parsed successfully, creating source code directory...");

        // Generate source code file.
        String outputDirectory = filename.split("/")[filename.split("/").length - 1].substring(0, filename.split("/")[filename.split("/").length - 1].indexOf('.'));
        // Generate output directory.
        System.out.println("Copying parser tools into the source code directory...");
        Runtime.getRuntime().exec("mkdir " + outputDirectory);
        // Copy necessary files into directory.
        Runtime.getRuntime().exec("cp ./Lexer.java ./" + outputDirectory);
        Runtime.getRuntime().exec("cp ./Token.java ./" + outputDirectory);
        Runtime.getRuntime().exec("cp ./TokenType.java ./" + outputDirectory);
        Runtime.getRuntime().exec("cp ./Tree.java ./" + outputDirectory);
        Runtime.getRuntime().exec("cp ./TreeGenerator.java ./" + outputDirectory);

        // Generate source code and save to new file.
        System.out.println("Generating the parser source code...");
        lexer.reset();
        String generatedSourceCode = parser.generateParser(lexer);
        System.out.println("Saving parser source code to output directory...");
        File file = new File(outputDirectory + "/Parser.java");
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(generatedSourceCode);
        fileWriter.close();
        System.out.println("Done.");
    }
}
