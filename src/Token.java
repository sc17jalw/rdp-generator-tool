public class Token {
    private String lexeme;
    private TokenType type;
    private Integer lineNumber;

    public Token(String lexeme, TokenType type, int lineNumber) {
        this.lexeme = lexeme;
        this.type = type;
        this.lineNumber = lineNumber;
    }

    @Override
    public boolean equals(Object other) {
        if (!((Token) other).getLexeme().equals(this.getLexeme())) {
            return false;
        } else if (!((Token) other).getType().equals(this.getType())) {
            return false;
        } else if (!(((Token) other).getLineNumber() == this.getLineNumber())) {
            return false;
        } else {
            return true;
        }
    }

    public String getLexeme() { return this.lexeme; }

    public TokenType getType() { return this.type; }

    public int getLineNumber() { return this.lineNumber; }

    public String toString() {
        return getType() + ": \"" + getLexeme() + "\" line: " + Integer.toString(getLineNumber());
    }
}