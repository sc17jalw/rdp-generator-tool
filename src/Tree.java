import java.util.ArrayList;

public class Tree {
    private ArrayList<Tree> children = new ArrayList<>();
    private String name;
    private Boolean nonTerminal;

    public Tree(String name, boolean nonTerminal) {
        this.name = name;
        this.nonTerminal = nonTerminal;
    }

    public ArrayList<Tree> getChildren() {
        return children;
    }

    public Boolean getNonTerminal() {
        return this.nonTerminal;
    }

    public Tree getChild(String childName) {
        Tree returnChild = null;
        for (Tree child : children) {
            if (child.toString().equals(childName)) {
                returnChild = child;
            }
        }
        return returnChild;
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public boolean equals(Object other) {
        if (other.getClass() == Tree.class) {
            return ((this.name.equals(other.toString())) && (this.getNonTerminal() == ((Tree) other).getNonTerminal()));
        } else {
            return false;
        }
    }

    public void addChild(String name, boolean nonTerminal) {
        children.add(new Tree(name, nonTerminal));
    }

    public void printTree(int indent) {
        // Print current node.
        if (nonTerminal) {
            // Non Terminal symbols printed red.
            System.out.println("\u001B[31m" + this.name + "\u001B[0m");
        } else {
            // Terminal symbols printed blue.
            System.out.println("\u001B[34m" + this.name + "\u001B[0m");
        }

        // Print tree for every child.
        indent++;
        for (Tree current : children) {
            for (int j = 0; j < indent; j++) {
                System.out.print("- ");
            }
            current.printTree(indent);
        }
    }

}
