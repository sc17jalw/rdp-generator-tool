import java.util.ArrayList;
import java.util.InputMismatchException;

public class Lexer {
    private ArrayList<Token> tokens = new ArrayList<>();
    private int position = 0;
    private int lineNumber = 1;

    public Token getNextToken() {
        return tokens.get(position++);
    }

    public Token peekNextToken(int ahead) {
        if ((position + ahead) == tokens.size()) {
            return new Token("endOfFile", TokenType.symbol, lineNumber);
        } else {
            return tokens.get(position + ahead);
        }
    }

    public void reset() { position = 0; }

    public ArrayList<Token> getTokens() {
        return tokens;
    }

    private void addToken(String lexeme, TokenType type, int lineNumber) {
        tokens.add(new Token(lexeme, type, lineNumber));
    }

    private void error(String message, int line) {
        throw new InputMismatchException(message + " on line: " + line);
    }

    public void tokenize(String inputText) {
        // Split up the string into characters.
        char[] inputChars = inputText.toCharArray();

        int i = 0;
        while (i < inputChars.length) {
            if (inputChars[i] == '*') {
                // Terminal tokens.
                i++;
                StringBuilder tokenString = new StringBuilder();
                while ((inputChars[i] != ' ') && (inputChars[i] != '\n') && (inputChars[i] != '\t')) {
                    tokenString.append(inputChars[i]);
                    i++;
                }
                addToken(tokenString.toString(), TokenType.terminal, lineNumber);
            } else if (inputChars[i] == '\"') {
                // Terminal strings.
                i++;
                StringBuilder tokenString = new StringBuilder();
                while (inputChars[i] != '\"') {
                    if (inputChars[i] == '\n') {
                        error("Terminal strings must be closed with a \" character.", lineNumber);
                    }
                    tokenString.append(inputChars[i]);
                    i++;
                }
                i++;
                addToken(tokenString.toString(), TokenType.terminalString, lineNumber);
            } else if (inputChars[i] == ':') {
                // Production rule.
                i++;
                if (inputChars[i] != '=') {
                    error("Production rules must be written as \":=\".", lineNumber);
                }
                i++;
                addToken(":=", TokenType.symbol, lineNumber);
            } else if ((inputChars[i] == '(') ||
                    (inputChars[i] == ')') ||
                    (inputChars[i] == '|') ||
                    (inputChars[i] == '{') ||
                    (inputChars[i] == '}') ||
                    (inputChars[i] == '[') ||
                    (inputChars[i] == ']') ||
                    (inputChars[i] == ';')) {
                addToken(Character.toString(inputChars[i]), TokenType.symbol, lineNumber);
                i++;
            } else if (inputChars[i] == '\n') {
                // New line.
                lineNumber++;
                i++;
            } else if ((inputChars[i] == ' ') || (inputChars[i] == '\t')) {
                // Move through whitespace.
                while ((inputChars[i] == ' ') || (inputChars[i] == '\t')) {
                    i++;
                }
            } else {
                // Non Terminal.
                StringBuilder tokenString = new StringBuilder();
                while ((inputChars[i] != ' ') && (inputChars[i] != '\n') && (inputChars[i] != '\t')) {
                    tokenString.append(inputChars[i]);
                    i++;
                }
                addToken(tokenString.toString(), TokenType.nonTerminal, lineNumber);
            }

        }

    }
}
