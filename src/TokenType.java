public enum TokenType {
    nonTerminal,
    terminal,
    terminalString,
    symbol,
}
