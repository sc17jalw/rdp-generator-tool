import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.util.*;

public class ParserGeneratorTest {
    @Test
    void testNoArguments() {
        String[] args = {};
        Assertions.assertThrows(FileNotFoundException.class, () -> ParserGenerator.main(args), "Please enter the filename of the grammar file in the form:\njava Generator *GRAMMAR FILE*");
    }

    @Test
    void timingTerminalStrings() {
        int MAX_ITERATIONS = 1000000;
        int NUMBER_FOR_AVERAGE = 100;
        for (int inputSize = 100000; inputSize <= MAX_ITERATIONS; inputSize += 100000) {
            GrammarParser parser = new GrammarParser();
            // Build string for input.
            String input = "non_terminal := \"test\" ;\n".repeat(Math.max(0, inputSize));

            // Generate list of times to take average.
            ArrayList<Long> timesTaken = new ArrayList<>();
            for (int i = 0; i < NUMBER_FOR_AVERAGE; i++) {
                long startTime = System.currentTimeMillis();
                Lexer lexer = new Lexer();
                lexer.tokenize(input);
                parser.generateParser(lexer);
                timesTaken.add(System.currentTimeMillis() - startTime);
            }

            // Calculate average speed.
            long totalSpeed = 0;
            for (Long time : timesTaken) {
                totalSpeed += time;
            }

            System.out.println("Size: " + inputSize + " - Time taken: " + (totalSpeed / NUMBER_FOR_AVERAGE));
        }
    }

    @Test
    void timingTestNonTerminals() {
        int MAX_ITERATIONS = 1000000;
        int NUMBER_FOR_AVERAGE = 100;
        for (int inputSize = 100000; inputSize <= MAX_ITERATIONS; inputSize += 100000) {
            GrammarParser parser = new GrammarParser();
            // Build string for problem input.
            StringBuilder input = new StringBuilder();
            for (int i = 0; i < inputSize; i++) {
                input.append("s").append(i).append(" := s").append(i + 1).append(" ;\n");
            }
            input.append("s").append(inputSize).append(" := \"test\" ;");

            // Generate list of times to take average.
            ArrayList<Long> timesTaken = new ArrayList<>();
            for (int i = 0; i < NUMBER_FOR_AVERAGE; i++) {
                long startTime = System.currentTimeMillis();
                Lexer lexer = new Lexer();
                lexer.tokenize(input.toString());
                parser.generateParser(lexer);
                timesTaken.add(System.currentTimeMillis() - startTime);
            }

            // Calculate average speed.
            long totalSpeed = 0;
            for (Long time : timesTaken) {
                totalSpeed += time;
            }

            System.out.println("Size: " + inputSize + " - Time taken: " + (totalSpeed / NUMBER_FOR_AVERAGE));
        }
    }

    @Test
    void timingTestRepeatedStatements() {
        int MAX_ITERATIONS = 1000;
        int NUMBER_FOR_AVERAGE = 100000;
        for (int inputSize = 100; inputSize <= MAX_ITERATIONS; inputSize += 100) {
            GrammarParser parser = new GrammarParser();
            // Build string for problem input.
            StringBuilder input = new StringBuilder();
            input.append("s1 := ");
            input.append("{ \"test\" ".repeat(Math.max(0, inputSize)));
            input.append("} ".repeat(Math.max(0, inputSize)));
            input.append(";");

            // Generate list of times to take average.
            ArrayList<Long> timesTaken = new ArrayList<>();
            for (int i = 0; i < NUMBER_FOR_AVERAGE; i++) {
                long startTime = System.nanoTime();
                Lexer lexer = new Lexer();
                lexer.tokenize(input.toString());
                parser.generateParser(lexer);
                timesTaken.add(System.nanoTime() - startTime);
            }

            // Calculate average speed.
            long totalSpeed = 0;
            for (Long time : timesTaken) {
                totalSpeed += time;
            }

            System.out.println("Size: " + inputSize + " - Time taken: " + (totalSpeed / NUMBER_FOR_AVERAGE));
        }
    }
}
