import org.junit.jupiter.api.Test;

public class TreeTest {
    @Test
    void testPrintTree() {
        Tree testTree = new Tree("start", true);

        testTree.addChild("child1", true);
        testTree.addChild("child2", true);
        testTree.getChildren().get(0).addChild("grandchild1", false);
        testTree.getChildren().get(0).addChild("grandchild2", false);
        testTree.getChildren().get(1).addChild("grandchild3", false);
        testTree.getChildren().get(1).addChild("grandchild4",true);
        testTree.getChildren().get(1).getChildren().get(1).addChild("grandchild5", false);

        testTree.printTree(0);
    }

    @Test
    void testAddChild() {
        Tree testTree = new Tree("start", true);
        assert(testTree.toString().equals("start"));

        testTree.addChild("test1", true);
        testTree.addChild("test2", true);
        testTree.getChildren().get(1).addChild("test3", false);

        assert(testTree.getChildren().get(0).toString().equals("test1"));
        assert(testTree.getChildren().get(1).toString().equals("test2"));
        assert(testTree.getChildren().get(1).getChildren().get(0).toString().equals("test3"));
    }

    @Test
    void testGetChild() {
        Tree testTree = new Tree("start", true);
        testTree.addChild("child", false);

        assert(testTree.getChild("child").equals(new Tree("child", false)));
    }
}
