package test_code.example;

import java.util.ArrayList;

public class Tree {
    private ArrayList<Tree> children = new ArrayList<>();
    private String name = "";

    public Tree(String name) {
        this.name = name;
    }

    public ArrayList<Tree> getChildren() {
        return children;
    }
    
    public Tree getChild(String childName) {
        Tree returnChild = null;
        for (Tree child : children) {
            if (child.toString().equals(childName)) {
                returnChild = child;
            }
        }
        return returnChild;
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public boolean equals(Object other) {
        if (other.getClass() == Tree.class)
            return this.name.equals(other.toString());
        else
            return false;
    }

    public void addChild(String name) {
        children.add(new Tree(name));
    }

    public void printTree(int indent) {
        // Print current node.
        System.out.println(this.name);
        // Print tree for every child.
        indent++;
        for (Tree current : children) {
            for (int j = 0; j < indent; j++) {
                System.out.print("- ");
            }
            current.printTree(indent);
        }
    }

}
