package test_code.example;

public enum TokenType {
    nonTerminal,
    terminal,
    terminalString,
    symbol,
    newLine
}
