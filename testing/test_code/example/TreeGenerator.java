package test_code.example;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TreeGenerator {
    public static void main(String[] args) {
        // Get filename
        String filename = "";
        try {
            if (!args[0].equals("")) {
                filename = args[0];
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            // No filename has been provided, exit the program.
            throw new ArrayIndexOutOfBoundsException("Please enter the filename of the language file to be parsed in the form:\njava Generator *GRAMMAR FILE*");
        }

        // Open file and read contents to a string.
        StringBuilder inputText = new StringBuilder();
        try {
            File languageFile = new File(filename);
            Scanner scanner = new Scanner(languageFile);
            while (scanner.hasNextLine()) {
                inputText.append(scanner.nextLine());
                inputText.append("\n");
            }
        } catch (FileNotFoundException e) {
            System.out.println("File: " + filename + " does not exist.");
            System.exit(1);
        }

        // Create lexer object and pass file text.
        Lexer lexer = new Lexer();
        lexer.tokenise(inputText.toString());

        // Initialise generated parser.
//        Parser parser = new Parser();
        language1ParserExample parser = new language1ParserExample();

        // Parse the language file.
        Tree outputTree = parser.parse(lexer);
        outputTree.printTree(0);
    }
}
