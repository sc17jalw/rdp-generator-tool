package test_code.example;

class language1ParserExample {
    private Lexer lexer;

    public Tree parse(Lexer lexer) {
        // Call start symbol.
        this.lexer = lexer;
        Tree outputTree = new Tree("s1");

//        while (!lexer.peekNextToken(0).getLexeme().equals("endOfFile")) {
            s1(outputTree);
//        }
        return outputTree;
    }

    private void s1(Tree outputTree) {
        // Terminal String.
        Token token = lexer.getNextToken();
        outputTree.addChild(token.getLexeme());

        // Conditional Statement.
        token = lexer.peekNextToken(0);
        if (token.getLexeme().equals("test2")) {
            token = lexer.getNextToken();
            outputTree.addChild(token.getLexeme());
        } else {
            token = lexer.getNextToken();
            outputTree.addChild(token.getLexeme());
        }

        // Non Terminal
        outputTree.addChild("s2");
        s2(outputTree.getChild("s2"));

        // Terminal String.
        token = lexer.getNextToken();
        outputTree.addChild(token.getLexeme());
    }

    private void s2(Tree outputTree) {
        // Terminal String.
        Token token = lexer.getNextToken();
        outputTree.addChild(token.getLexeme());

        // Repeated statement.
        while (lexer.peekNextToken(0).getLexeme().equals(",")) {
            token = lexer.getNextToken();
            outputTree.addChild(token.getLexeme());

            // Terminal.
            token = lexer.getNextToken();
            outputTree.addChild(token.getLexeme());
        }

        // Terminal String.
        token = lexer.getNextToken();
        outputTree.addChild(token.getLexeme());

        // Optional Statement.
        token = lexer.peekNextToken(0);
        if (token.getLexeme().equals("test8")) {
            token = lexer.getNextToken();
            outputTree.addChild(token.getLexeme());
        }
    }
}