import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TokenTest {
    private Token token = new Token("test", TokenType.nonTerminal, 8);

    @Test
    void testGetLexeme() {
        assert(token.getLexeme().equals("test"));
    }

    @Test
    void testGetType() {
        assert(token.getType().equals(TokenType.nonTerminal));
    }

    @Test
    void testGetLineNumber() {
        assert(token.getLineNumber() == 8);
    }
}

