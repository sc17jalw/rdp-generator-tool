import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.InputMismatchException;

public class GrammarParserTest {
    @Test
    void testCorrectInput() {
        Lexer lexer = new Lexer();
        lexer.tokenize("s1 := \"test1\" ( \"test2\" | \"test3\" ) s2 \"end\";\ns2 := \"test4\" { \",\" *string } \";\" [ \"test8\" ];\n");

        GrammarParser parser = new GrammarParser();
        parser.parseGrammar(lexer);
    }

    @Test
    void testIncorrectInput() {
        Lexer lexer = new Lexer();
        lexer.tokenize("s1 := \"test1\" ( \"test2\" | \"test3\" ) s2 \"end\"\ns2 := \"test4\" \",\" *string } \";\" [ \"test8\" ]\n");

        GrammarParser parser = new GrammarParser();
        Assertions.assertThrows(InputMismatchException.class, () -> parser.parseGrammar(lexer), "ERROR: Expected new line character at end of production rule. TOKEN: symbol: } line: 2");
    }

    @Test
    void testGeneratedOutput1() {
        Lexer lexer = new Lexer();
        lexer.tokenize("s1 := \"test1\" ( \"test2\" | \"test3\" ) s2 \"end\";\ns2 := \"test4\" { \",\" *string } \";\" [ \"test8\" ];\n");

        GrammarParser parser = new GrammarParser();
        lexer.reset();
        if (!parser.generateParser(lexer).equals("class Parser {\n" +
                "\tprivate Lexer lexer;\n" +
                "\n" +
                "\tprivate void error(Token token, String message) {\n" +
                "\t\tSystem.out.println(\"ERROR: \" + message);\n" +
                "\t\tSystem.out.println(\"TOKEN: \" + token.toString());\n" +
                "\t\tSystem.exit(1);\n" +
                "\t}\n" +
                "\n" +
                "\tpublic Tree parse(Lexer lexer) {\n" +
                "\t\t// Call start symbol.\n" +
                "\t\tthis.lexer = lexer;\n" +
                "\t\tTree outputTree = new Tree(\"s1\", true);\n" +
                "\t\ts1(outputTree);\n" +
                "\t\treturn outputTree;\n" +
                "\t}\n" +
                "\n" +
                "\t// Production rule.\n" +
                "\tprivate void s1(Tree outputTree) {\n" +
                "\t\tToken token = null;\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"test1\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"test1\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Conditional Statement.\n" +
                "\t\ttoken = lexer.peekNextToken(0);\n" +
                "\t\tif (token.getLexeme().equals(\"test2\")) {\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"test2\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"test2\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t} else {\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"test3\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"test3\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Non terminal\n" +
                "\t\toutputTree.addChild(\"s2\", true);\n" +
                "\t\ts2(outputTree.getChild(\"s2\"));\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"end\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"end\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t}\n" +
                "\n" +
                "\t// Production rule.\n" +
                "\tprivate void s2(Tree outputTree) {\n" +
                "\t\tToken token = null;\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"test4\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"test4\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Repeated Statement.\n" +
                "\t\twhile (lexer.peekNextToken(0).getLexeme().equals(\",\")) {\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\",\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\",\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Terminal.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\";\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\";\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Optional Statement.\n" +
                "\t\ttoken = lexer.peekNextToken(0);\n" +
                "\t\tif (token.getLexeme().equals(\"test8\")) {\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"test8\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"test8\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t}\n" +
                "\n" +
                "\t}\n" +
                "\n" +
                "}")) throw new AssertionError();
    }

    @Test
    void testGeneratedOutput2() {
        Lexer lexer = new Lexer();
        lexer.tokenize("class_declaration := \"class\" *identifier \"{\" { \"member\" member_declaration } \"}\" ;\n" +
                "member_declaration := variable_declaration subroutine ;\n" +
                "variable_declaration := [ \"var\" *type *identifier { \",\" *identifier } \";\" ] ;\n" +
                "subroutine := [ \"func\" *type *method_name \"(\" *type *identifier { \",\" *type *identifier } \")\" subroutine_body ] ;\n" +
                "subroutine_body := \"{\" { \"line\" statement control_statement } \"}\" ;\n" +
                "statement := [ \"statement\" variable_declaration assignment_statement ] ;\n" +
                "assignment_statement := [ \"assign\" *identifier \"=\" *value \";\" ] ;\n" +
                "control_statement := [ \"control\" if_statement while_statement ] ;\n" +
                "if_statement := [ \"if\" \"(\" condition \")\" subroutine_body [ \"else\" subroutine_body ] ] ;\n" +
                "while_statement := [ \"while\" \"(\" condition \")\" subroutine_body ] ;\n" +
                "condition := *identifier \"==\" *identifier ;\n");

        GrammarParser parser = new GrammarParser();
        parser.parseGrammar(lexer);

        lexer.reset();
        if (!parser.generateParser(lexer).equals("class Parser {\n" +
                "\tprivate Lexer lexer;\n" +
                "\n" +
                "\tprivate void error(Token token, String message) {\n" +
                "\t\tSystem.out.println(\"ERROR: \" + message);\n" +
                "\t\tSystem.out.println(\"TOKEN: \" + token.toString());\n" +
                "\t\tSystem.exit(1);\n" +
                "\t}\n" +
                "\n" +
                "\tpublic Tree parse(Lexer lexer) {\n" +
                "\t\t// Call start symbol.\n" +
                "\t\tthis.lexer = lexer;\n" +
                "\t\tTree outputTree = new Tree(\"class_declaration\", true);\n" +
                "\t\tclass_declaration(outputTree);\n" +
                "\t\treturn outputTree;\n" +
                "\t}\n" +
                "\n" +
                "\t// Production rule.\n" +
                "\tprivate void class_declaration(Tree outputTree) {\n" +
                "\t\tToken token = null;\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"class\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"class\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Terminal.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"{\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"{\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Repeated Statement.\n" +
                "\t\twhile (lexer.peekNextToken(0).getLexeme().equals(\"member\")) {\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"member\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"member\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Non terminal\n" +
                "\t\toutputTree.addChild(\"member_declaration\", true);\n" +
                "\t\tmember_declaration(outputTree.getChild(\"member_declaration\"));\n" +
                "\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"}\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"}\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t}\n" +
                "\n" +
                "\t// Production rule.\n" +
                "\tprivate void member_declaration(Tree outputTree) {\n" +
                "\t\tToken token = null;\n" +
                "\t\t// Non terminal\n" +
                "\t\toutputTree.addChild(\"variable_declaration\", true);\n" +
                "\t\tvariable_declaration(outputTree.getChild(\"variable_declaration\"));\n" +
                "\n" +
                "\t\t// Non terminal\n" +
                "\t\toutputTree.addChild(\"subroutine\", true);\n" +
                "\t\tsubroutine(outputTree.getChild(\"subroutine\"));\n" +
                "\n" +
                "\t}\n" +
                "\n" +
                "\t// Production rule.\n" +
                "\tprivate void variable_declaration(Tree outputTree) {\n" +
                "\t\tToken token = null;\n" +
                "\t\t// Optional Statement.\n" +
                "\t\ttoken = lexer.peekNextToken(0);\n" +
                "\t\tif (token.getLexeme().equals(\"var\")) {\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"var\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"var\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Terminal.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\n" +
                "\t\t// Terminal.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\n" +
                "\t\t// Repeated Statement.\n" +
                "\t\twhile (lexer.peekNextToken(0).getLexeme().equals(\",\")) {\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\",\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\",\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Terminal.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\";\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\";\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t}\n" +
                "\n" +
                "\t}\n" +
                "\n" +
                "\t// Production rule.\n" +
                "\tprivate void subroutine(Tree outputTree) {\n" +
                "\t\tToken token = null;\n" +
                "\t\t// Optional Statement.\n" +
                "\t\ttoken = lexer.peekNextToken(0);\n" +
                "\t\tif (token.getLexeme().equals(\"func\")) {\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"func\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"func\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Terminal.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\n" +
                "\t\t// Terminal.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"(\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"(\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Terminal.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\n" +
                "\t\t// Terminal.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\n" +
                "\t\t// Repeated Statement.\n" +
                "\t\twhile (lexer.peekNextToken(0).getLexeme().equals(\",\")) {\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\",\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\",\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Terminal.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\n" +
                "\t\t// Terminal.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\")\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\")\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Non terminal\n" +
                "\t\toutputTree.addChild(\"subroutine_body\", true);\n" +
                "\t\tsubroutine_body(outputTree.getChild(\"subroutine_body\"));\n" +
                "\n" +
                "\t\t}\n" +
                "\n" +
                "\t}\n" +
                "\n" +
                "\t// Production rule.\n" +
                "\tprivate void subroutine_body(Tree outputTree) {\n" +
                "\t\tToken token = null;\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"{\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"{\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Repeated Statement.\n" +
                "\t\twhile (lexer.peekNextToken(0).getLexeme().equals(\"line\")) {\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"line\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"line\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Non terminal\n" +
                "\t\toutputTree.addChild(\"statement\", true);\n" +
                "\t\tstatement(outputTree.getChild(\"statement\"));\n" +
                "\n" +
                "\t\t// Non terminal\n" +
                "\t\toutputTree.addChild(\"control_statement\", true);\n" +
                "\t\tcontrol_statement(outputTree.getChild(\"control_statement\"));\n" +
                "\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"}\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"}\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t}\n" +
                "\n" +
                "\t// Production rule.\n" +
                "\tprivate void statement(Tree outputTree) {\n" +
                "\t\tToken token = null;\n" +
                "\t\t// Optional Statement.\n" +
                "\t\ttoken = lexer.peekNextToken(0);\n" +
                "\t\tif (token.getLexeme().equals(\"statement\")) {\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"statement\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"statement\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Non terminal\n" +
                "\t\toutputTree.addChild(\"variable_declaration\", true);\n" +
                "\t\tvariable_declaration(outputTree.getChild(\"variable_declaration\"));\n" +
                "\n" +
                "\t\t// Non terminal\n" +
                "\t\toutputTree.addChild(\"assignment_statement\", true);\n" +
                "\t\tassignment_statement(outputTree.getChild(\"assignment_statement\"));\n" +
                "\n" +
                "\t\t}\n" +
                "\n" +
                "\t}\n" +
                "\n" +
                "\t// Production rule.\n" +
                "\tprivate void assignment_statement(Tree outputTree) {\n" +
                "\t\tToken token = null;\n" +
                "\t\t// Optional Statement.\n" +
                "\t\ttoken = lexer.peekNextToken(0);\n" +
                "\t\tif (token.getLexeme().equals(\"assign\")) {\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"assign\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"assign\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Terminal.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"=\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"=\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Terminal.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\";\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\";\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t}\n" +
                "\n" +
                "\t}\n" +
                "\n" +
                "\t// Production rule.\n" +
                "\tprivate void control_statement(Tree outputTree) {\n" +
                "\t\tToken token = null;\n" +
                "\t\t// Optional Statement.\n" +
                "\t\ttoken = lexer.peekNextToken(0);\n" +
                "\t\tif (token.getLexeme().equals(\"control\")) {\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"control\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"control\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Non terminal\n" +
                "\t\toutputTree.addChild(\"if_statement\", true);\n" +
                "\t\tif_statement(outputTree.getChild(\"if_statement\"));\n" +
                "\n" +
                "\t\t// Non terminal\n" +
                "\t\toutputTree.addChild(\"while_statement\", true);\n" +
                "\t\twhile_statement(outputTree.getChild(\"while_statement\"));\n" +
                "\n" +
                "\t\t}\n" +
                "\n" +
                "\t}\n" +
                "\n" +
                "\t// Production rule.\n" +
                "\tprivate void if_statement(Tree outputTree) {\n" +
                "\t\tToken token = null;\n" +
                "\t\t// Optional Statement.\n" +
                "\t\ttoken = lexer.peekNextToken(0);\n" +
                "\t\tif (token.getLexeme().equals(\"if\")) {\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"if\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"if\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"(\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"(\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Non terminal\n" +
                "\t\toutputTree.addChild(\"condition\", true);\n" +
                "\t\tcondition(outputTree.getChild(\"condition\"));\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\")\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\")\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Non terminal\n" +
                "\t\toutputTree.addChild(\"subroutine_body\", true);\n" +
                "\t\tsubroutine_body(outputTree.getChild(\"subroutine_body\"));\n" +
                "\n" +
                "\t\t// Optional Statement.\n" +
                "\t\ttoken = lexer.peekNextToken(0);\n" +
                "\t\tif (token.getLexeme().equals(\"else\")) {\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"else\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"else\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Non terminal\n" +
                "\t\toutputTree.addChild(\"subroutine_body\", true);\n" +
                "\t\tsubroutine_body(outputTree.getChild(\"subroutine_body\"));\n" +
                "\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t}\n" +
                "\n" +
                "\t}\n" +
                "\n" +
                "\t// Production rule.\n" +
                "\tprivate void while_statement(Tree outputTree) {\n" +
                "\t\tToken token = null;\n" +
                "\t\t// Optional Statement.\n" +
                "\t\ttoken = lexer.peekNextToken(0);\n" +
                "\t\tif (token.getLexeme().equals(\"while\")) {\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"while\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"while\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"(\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"(\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Non terminal\n" +
                "\t\toutputTree.addChild(\"condition\", true);\n" +
                "\t\tcondition(outputTree.getChild(\"condition\"));\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\")\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\")\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Non terminal\n" +
                "\t\toutputTree.addChild(\"subroutine_body\", true);\n" +
                "\t\tsubroutine_body(outputTree.getChild(\"subroutine_body\"));\n" +
                "\n" +
                "\t\t}\n" +
                "\n" +
                "\t}\n" +
                "\n" +
                "\t// Production rule.\n" +
                "\tprivate void condition(Tree outputTree) {\n" +
                "\t\tToken token = null;\n" +
                "\t\t// Terminal.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\n" +
                "\t\t// Terminal String.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\tif (token.getLexeme().equals(\"==\")) {\n" +
                "\t\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\t\t} else {\n" +
                "\t\t\terror(token, \"Expected symbol \\\"==\\\".\");\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t// Terminal.\n" +
                "\t\ttoken = lexer.getNextToken();\n" +
                "\t\toutputTree.addChild(token.getLexeme(), false);\n" +
                "\n" +
                "\t}\n" +
                "\n" +
                "}")) throw new AssertionError();
    }
}
