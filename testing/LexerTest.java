import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class LexerTest {
    Lexer lexer = new Lexer();

    @Test
    void testTokeniser() {
        lexer.tokenize("s1 := \"test1\" ( \"test2\" | \"test3\" ) s2 \"end\" ;\ns2 := \"test4\" { \",\" *string } \";\" [ \"test8\" ] ;\n");
        // Create test array to compare with generated array.
        ArrayList<Token> testTokens = new ArrayList<>();
        testTokens.add(new Token("s1", TokenType.nonTerminal, 1));
        testTokens.add(new Token(":=", TokenType.symbol, 1));
        testTokens.add(new Token("test1", TokenType.terminalString, 1));
        testTokens.add(new Token("(", TokenType.symbol, 1));
        testTokens.add(new Token("test2", TokenType.terminalString, 1));
        testTokens.add(new Token("|", TokenType.symbol, 1));
        testTokens.add(new Token("test3", TokenType.terminalString, 1));
        testTokens.add(new Token(")", TokenType.symbol, 1));
        testTokens.add(new Token("s2", TokenType.nonTerminal, 1));
        testTokens.add(new Token("end", TokenType.terminalString, 1));
        testTokens.add(new Token(";", TokenType.symbol, 1));

        testTokens.add(new Token("s2", TokenType.nonTerminal, 2));
        testTokens.add(new Token(":=", TokenType.symbol, 2));
        testTokens.add(new Token("test4", TokenType.terminalString, 2));
        testTokens.add(new Token("{", TokenType.symbol, 2));
        testTokens.add(new Token(",", TokenType.terminalString, 2));
        testTokens.add(new Token("string", TokenType.terminal, 2));
        testTokens.add(new Token("}", TokenType.symbol, 2));
        testTokens.add(new Token(";", TokenType.terminalString, 2));
        testTokens.add(new Token("[", TokenType.symbol, 2));
        testTokens.add(new Token("test8", TokenType.terminalString, 2));
        testTokens.add(new Token("]", TokenType.symbol, 2));
        testTokens.add(new Token(";", TokenType.symbol, 2));

        int i = 0;
        for (Token item : testTokens) {
            assert(item.equals(lexer.getTokens().get(i)));
            i++;
        }
    }

    @Test
    void testGetNextToken() {
        lexer.tokenize("s1 := \"test1\" ( \"test2\" | \"test3\" ) s2 \"end\"\ns2 := \"test4\" { \",\" *string } \";\" [ \"test8\" ]\n");
        Token token = lexer.getNextToken();
        assert(token.equals(new Token("s1", TokenType.nonTerminal, 1)));
        token = lexer.getNextToken();
        assert(token.equals(new Token(":=", TokenType.symbol, 1)));
        token = lexer.getNextToken();
        assert(token.equals(new Token("test1", TokenType.terminalString, 1)));
    }

    @Test
    void testTokeniser2() {
        lexer.tokenize("\n\n    \n\n  \n\"test\"");
        Token testToken = lexer.getNextToken();
        assert(testToken.equals(new Token("test", TokenType.terminalString, 6)));
    }

    @Test
    void testReset() {
        lexer.tokenize("s1 := \"test1\" ( \"test2\" | \"test3\" ) s2 \"end\"\ns2 := \"test4\" { \",\" *string } \";\" [ \"test8\" ]\n");
        Token token = lexer.getNextToken();
        token = lexer.getNextToken();
        token = lexer.getNextToken();
        lexer.reset();
        token = lexer.getNextToken();
        assert(token.equals(new Token("s1", TokenType.nonTerminal, 1)));
    }
}
