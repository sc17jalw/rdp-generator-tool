class Parser {
	private Lexer lexer;

	private void error(Token token, String message) {
		System.out.println("ERROR: " + message);
		System.out.println("TOKEN: " + token.toString());
		System.exit(1);
	}

	public Tree parse(Lexer lexer) {
		// Call start symbol.
		this.lexer = lexer;
		Tree outputTree = new Tree("s1", true);
		s1(outputTree);
		return outputTree;
	}

	// Production rule.
	private void s1(Tree outputTree) {
		Token token = null;
		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("test1")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"test1\".");
		}

		// Conditional Statement.
		token = lexer.peekNextToken(0);
		if (token.getLexeme().equals("test2")) {

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("test2")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"test2\".");
		}

		} else {

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("test3")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"test3\".");
		}

		}

		// Non terminal
		outputTree.addChild("s2", true);
		s2(outputTree.getChild("s2"));

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("end")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"end\".");
		}

	}

	// Production rule.
	private void s2(Tree outputTree) {
		Token token = null;
		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("test4")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"test4\".");
		}

		// Repeated Statement.
		while (lexer.peekNextToken(0).getLexeme().equals(",")) {

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals(",")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \",\".");
		}

		// Terminal.
		token = lexer.getNextToken();
		outputTree.addChild(token.getLexeme(), false);

		}

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals(";")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \";\".");
		}

		// Optional Statement.
		token = lexer.peekNextToken(0);
		if (token.getLexeme().equals("test8")) {

		// Terminal String.
		token = lexer.getNextToken();
		if (token.getLexeme().equals("test8")) {
			outputTree.addChild(token.getLexeme(), false);
		} else {
			error(token, "Expected symbol \"test8\".");
		}

		}

	}

}